# docker-cassandra
Dockers for Cassandra Clusters, using self-discovery (GossipingPropertyFileSnitch) vNodes.

#Included Dockers:
cassandra-base: Baseline of cassandra 2.1.3.
cassandra-master: Join and Seed.
cassandra-base: Worker only nodes.

# Defaults:

File: cassandra-master.sh

CASSANDRA_SEEDS=$IP
CASSANDRA_CLUSTERNAME=${CASSANDRA_CLUSTER:-"Default Cluster"}
CASSANDRA_TOKENS=${CASSANDRA_TOKENS:-256}
CASSANDRA_DC="DC1"
CASSANDRA_RACK="RACK1"
CASSANDRA_STORE_DIR="/store"

File: cassandra-worker.sh

CASSANDRA_SEEDS
CASSANDRA_CLUSTERNAME=${CASSANDRA_CLUSTER:-"Default Cluster"}
CASSANDRA_TOKENS=${CASSANDRA_TOKENS:-256}
CASSANDRA_DC="DC1"
CASSANDRA_RACK="RACK1"
CASSANDRA_STORE_DIR="/store"

# Howto:
0- Install and enable Docker.
apt-get -y update && \
apt-get -y install docker.io

1- Get the docker Java8 and Cassandra.
git clone http://bitbucket.org/victorock/docker-oracle-java8
git clone http://bitbucket.org/victorock/docker-cassandra

2- Configure environment variables inside of worker/master Dockerfile.

ENV CASSANDRA_CONFIG /etc/cassandra
ENV CASSANDRA_CLUSTERNAME "cluster1"
ENV CASSANDRA_TOKENS 256
ENV CASSANDRA_DC "DC1"
ENV CASSANDRA_RACK "RACK1"
ENV CASSANDRA_STORE /store

3- Build the dockers.
docker build -t="raisonata/java:oracle-java8" ./docker-oracle-java8
docker build -t="raisonata/cassandra:base" ./docker-cassandra/cassandra-base/
docker build -t="raisonata/cassandra:master" ./docker-cassandra/cassandra-master/
docker build -t="raisonata/cassandra:worker" ./docker-cassandra/cassandra-worker/

4- Run the dockers.
docker run -d  --name "node01.example" -h "node01.example.homelab.local" raisonata/cassandra:master
docker run -d  --name "node02.example" -h "node02.example.raisonata.fr" raisonata/cassandra:worker <node01_ip> CASSANDRA_DC=DC1 CASSANDRA_RACK=RACK1

docker run -d  --name "node03.example" -h "node03.example.raisonata.fr" raisonata/cassandra:master <node01_ip> CASSANDRA_DC=DC1 CASSANDRA_RACK=RACK2
docker run -d  --name "node04.example" -h "node04.example.raisonata.fr" raisonata/cassandra:worker <node01_ip> CASSANDRA_DC=DC1 CASSANDRA_RACK=RACK2

docker run -d  --name "node05.example" -h "node05.example.raisonata.fr" raisonata/cassandra:master <node01_ip> CASSANDRA_DC=DC1 CASSANDRA_RACK=RACK3
docker run -d  --name "node06.example" -h "node06.example.raisonata.fr" raisonata/cassandra:worker <node01_ip> CASSANDRA_DC=DC1 CASSANDRA_RACK=RACK3

docker run -d  --name "node06.example" -h "node07.example.homelab.local" raisonata/cassandra:master <node01_ip> CASSANDRA_DC=DC2 CASSANDRA_RACK=RACK1
docker run -d  --name "node07.example" -h "node08.example.raisonata.fr" raisonata/cassandra:worker <node01_ip>,<node07_ip> CASSANDRA_DC=DC2 CASSANDRA_RACK=RACK1

docker run -d  --name "node08.example" -h "node09.example.raisonata.fr" raisonata/cassandra:master <node01_ip>,<node07_ip> CASSANDRA_DC=DC2 CASSANDRA_RACK=RACK2
docker run -d  --name "node09.example" -h "node10.example.raisonata.fr" raisonata/cassandra:worker <node01_ip>,<node07_ip> CASSANDRA_DC=DC2 CASSANDRA_RACK=RACK2

docker run -d  --name "node10.example" -h "node11.example.raisonata.fr" raisonata/cassandra:master <node01_ip>,<node07_ip> CASSANDRA_DC=DC2 CASSANDRA_RACK=RACK3
docker run -d  --name "node11.example" -h "node12.example.raisonata.fr" raisonata/cassandra:worker <node01_ip>,<node07_ip> CASSANDRA_DC=DC2 CASSANDRA_RACK=RACK3

#Tips
Get docker IP: 
docker inspect <node_name> | grep IPA | cut -d'"' -f 4

Automate:
docker run -d  --name "node01.example" -h "node01.example.homelab.local" raisonata/cassandra:master
docker run -d  --name "node02.example" -h "node02.example.raisonata.fr" raisonata/cassandra:worker `docker inspect node01.example | grep IPA | cut -d'"' -f 4` CASSANDRA_DC=DC1 CASSANDRA_RACK=RACK1

