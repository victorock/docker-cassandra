#!/bin/bash
# Get running container's IP
IP=`hostname --ip-address | cut -f 1 -d ' '`

export CASSANDRA_CONFIG=${CASSANDRA_CONFIG:-"/etc/cassandra"}
export CASSANDRA_CLUSTERNAME=${CASSANDRA_CLUSTER:-"Default Cluster"}
export CASSANDRA_TOKENS=${CASSANDRA_TOKENS:-256}
export CASSANDRA_DC=${CASSANDRA_DC:-"DC1"}
export CASSANDRA_RACK=${CASSANDRA_RAC:-"RACK1"}
export CASSANDRA_STORE_DIR=${CASSANDRA_STORE_DIR:-"/store"}
export CASSANDRA_STORE_COMMITLOG=${CASSANDRA_STORE_COMMITLOG:-"${CASSANDRA_STORE_DIR}/commitlog"}
export CASSANDRA_STORE_DATA=${CASSANDRA_STORE_DATA:-"${CASSANDRA_STORE_DIR}/data"}
export CASSANDRA_STORE_SAVED_CACHES=${CASSANDRA_STORE_SAVED_CACHES:-"${CASSANDRA_STORE_DIR}/saved_caches"}
export CASSANDRA_STORE_LOGS=${CASSANDRA_STORE_LOGS:-"${CASSANDRA_STORE_DIR}/logs"}

# Storage
mkdir -p "${CASSANDRA_STORE_DIR}"
mkdir -p "${CASSANDRA_STORE_COMMITLOG}"
mkdir -p "${CASSANDRA_STORE_DATA}"
mkdir -p "${CASSANDRA_STORE_SAVED_CACHES}"
mkdir -p "${CASSANDRA_STORE_LOGS}"