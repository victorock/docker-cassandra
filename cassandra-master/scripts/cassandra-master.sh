#!/usr/bin/env bash
. /usr/local/sbin/cassandra-common

if [ $# -ge 1 ]; then CASSANDRA_SEEDS="$1,$IP"; 
else CASSANDRA_SEEDS="$IP"; fi

# Cluster Name
sed -i -e "s/^cluster_name.*/cluster_name: \'$CASSANDRA_CLUSTERNAME\'/" $CASSANDRA_CONFIG/cassandra.yaml

# Give our IP as RPC
sed -i -e "s/^rpc_address.*/rpc_address: $IP/" $CASSANDRA_CONFIG/cassandra.yaml

# Give our IP as seeder
sed -i -e "s/^broadcast_rpc_address.*/broadcast_rpc_address: $IP/" $CASSANDRA_CONFIG/cassandra.yaml

# Listen on IP:port of the container
sed -i -e "s/^listen_address.*/listen_address: $IP/" $CASSANDRA_CONFIG/cassandra.yaml

# Most likely not needed
#echo "JVM_OPTS=\"\$JVM_OPTS -Djava.rmi.server.hostname=$IP\"" >> $CASSANDRA_CONFIG/cassandra-env.sh

# Be your own seed
sed -i -e "s/- seeds: \"127.0.0.1\"/- seeds: \"$CASSANDRA_SEEDS\"/" $CASSANDRA_CONFIG/cassandra.yaml

# Setting amount of vnodes
sed -i -e "s/^num_tokens.*/num_tokens: $CASSANDRA_TOKENS/" $CASSANDRA_CONFIG/cassandra.yaml

# Configuring cluster for Gossip
sed -i -e "s/endpoint_snitch.*/endpoint_snitch: GossipingPropertyFileSnitch/" $CASSANDRA_CONFIG/cassandra.yaml

# Configuring directory for persistent data
sed -i -e "s,/var/lib/cassandra,$CASSANDRA_STORE_DIR," $CASSANDRA_CONFIG/cassandra.yaml

# Setting Topology, DC and RACK
echo "dc=$CASSANDRA_DC" > $CASSANDRA_CONFIG/cassandra-rackdc.properties
echo "rack=$CASSANDRA_RACK" >> $CASSANDRA_CONFIG/cassandra-rackdc.properties

cassandra -f
